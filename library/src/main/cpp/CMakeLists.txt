# the minimum version of CMake.
cmake_minimum_required(VERSION 3.4.1)
project(tar)

set(NATIVERENDER_ROOT_PATH ${CMAKE_CURRENT_SOURCE_DIR})

add_subdirectory(tar)
add_subdirectory(boundscheck)

include_directories(${NATIVERENDER_ROOT_PATH}
                    ${NATIVERENDER_ROOT_PATH}/tar/tar
                    )

include_directories(${NATIVERENDER_ROOT_PATH}
                    ${NATIVERENDER_ROOT_PATH}/boundscheck/third_party_bounds_checking_function/include
                    ${NATIVERENDER_ROOT_PATH}/boundscheck/third_party_bounds_checking_function/src
                    )

add_library(tar SHARED ohos_tar.cpp)

target_link_libraries(tar PUBLIC libace_napi.z.so libhilog_ndk.z.so ctar boundscheck -s libc++.a)

